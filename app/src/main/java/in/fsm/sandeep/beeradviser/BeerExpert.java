package in.fsm.sandeep.beeradviser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandeep on 8/8/16.
 */
public class BeerExpert {

    public List<String> getBrands(String color){
        List<String> brands = new ArrayList<String>();
        if(color.equals("amber")){
            brands.add("Jack Amber");
            brands.add("Red Moose");
        }else{
            brands.add("Jail Pale ale");
            brands.add("Goose Island");
        }

        return brands;
    }


}
