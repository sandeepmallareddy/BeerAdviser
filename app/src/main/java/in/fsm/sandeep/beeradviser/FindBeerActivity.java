package in.fsm.sandeep.beeradviser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class FindBeerActivity extends AppCompatActivity {

    protected BeerExpert expert = new BeerExpert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_beer);
    }


    public void onClickFindBeer(View view){
        TextView brands = (TextView) findViewById(R.id.brands);
        Spinner color = (Spinner) findViewById(R.id.color);
        String selectedColor = (String) color.getSelectedItem();
        Log.v("OnClickFindBeer",selectedColor);

        List<String> choices = expert.getBrands(selectedColor);
        //StringBuilder brandsFormatted = new StringBuilder();
        String brandsstring = "";
        for(String choice:choices){
            brandsstring+=choice+"\n";
            //brandsFormatted.append(choice).append('\n');
        }

        brands.setText(brandsstring);
    }



}
